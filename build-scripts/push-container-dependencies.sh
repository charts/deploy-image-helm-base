#!/usr/bin/env bash

set -e

GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

display_task "Pushing Container Dependencies"
for image in $("${GL_MP_LIST_IMAGES}"); do
    source_image_name="$(echo "${image##*/}" | cut -d':' -f1 | cut -d'@' -f1)"
    source_image_tag="${image//*:}"
    mirrored_image="${GCR_REGISTRY}/${source_image_name}:${source_image_tag}"

    if docker pull "${image}" > /dev/null 2>&1; then
        display_success "Pulled ${image} to local container registry"
    else
        display_failure "Unable to pull ${image} to local container registry"
    fi

    if docker tag "${image}" "${mirrored_image}" > /dev/null 2>&1; then
        display_success "Created tag ${mirrored_image} referring to ${image}"
    else
        display_failure "Unable to create tag ${mirrored_image} referring to ${image}"
    fi

    if docker push "${mirrored_image}" > /dev/null 2>&1; then
        display_success "Pushed ${mirrored_image}"
    else
        display_failure "Unable to push ${mirrored_image}"
    fi
done
