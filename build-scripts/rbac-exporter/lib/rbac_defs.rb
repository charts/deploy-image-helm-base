# Helper Module to abstract away specific functionality

module KubeObjectDefinitions
  # Defines the text strings that define entry types that are related to
  # role based access control (RBAC)
  def self.rbac_kinds
    %w(ServiceAccount RoleBinding Role ClusterRoleBinding ClusterRole)
  end

  # Returns an array chart objects that qualify as Service Accounts
  def self.subjects?(chart_objects = nil)
    return [] if chart_objects.nil?

    chart_objects.select { |x| x&.key?("kind") && x["kind"] == "ServiceAccount" }
  end

  # Returns an array of chart objects that are defined as role bindings
  def self.role_bindings?(chart_objects = nil)
    return [] if chart_objects.nil?

    chart_objects.select { |x| x&.key?("kind") && (%w(RoleBinding ClusterRoleBinding).include? x["kind"]) }
  end

  # Returns an array of chart objects that are defined as roles
  def self.roles?(chart_objects = nil)
    return [] if chart_objects.nil?

    chart_objects.select { |x| x&.key?("kind") && (%w(Role ClusterRole).include? x["kind"]) }
  end
end
