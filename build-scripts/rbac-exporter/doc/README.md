## Script Usage

Users should have a yaml file created by running `helm template`.

Pass the path to this yaml file to `convert\_chart\_roles\_to\_schema.rb`
to generate ***YAML*** formatted output appropriate for insertion to the
Google GKE Marketplace schema.yaml file.

## Script Requirements

The script only requires an install of ruby and has been tested against
version 2.5.x.

## Running Tests on the Library

Install the bundler gem if it is not already installed with
`gem install bundler`.

Install rspec from the ***rbac-exporter*** directory using
`bundle install --binstubs`.

Run tests using `./bin/rspec --format=doc` to see all expected
behaviors and supported features.
