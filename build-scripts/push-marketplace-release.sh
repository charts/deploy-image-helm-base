#!/usr/bin/env bash

# Import build environment variables and shared functions
GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

display_task "Pushing deployer containers"
for tag in $(get_container_tags "REMOTE_ONLY"); do
    if docker push "${tag}" > /dev/null 2>&1; then
        display_success "Pushed ${tag}"
    else
        display_failure "Failed to push ${tag}"
    fi
done

display_task "Push complete"
