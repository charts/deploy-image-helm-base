#!/usr/bin/env bash

# Import build environment variables and shared functions
GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

function cleanup() {
    kubectl -n "${GL_MP_NAMESPACE}" get ingress,svc,pdb,hpa,deploy,statefulset,job,pod,secret,configmap,pvc,secret,clusterrole,clusterrolebinding,role,rolebinding,sa,applications 2>&1 \
      | grep "${GL_MP_APP_INSTANCE_NAME}" \
      | awk '{print $1}' \
      | xargs kubectl -n "${GL_MP_NAMESPACE}" delete \
      || true
}

display_task "Starting test environment tear down"
if cleanup; then
    display_success "...completed successfully"
else
    display_failure "...tear down failed"
fi
