#!/usr/bin/env bash
# build-release.sh
#
# Builds a release for GKE Marketplace
# expects no arguments

set -eo pipefail

# Import Environment Setup
GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

################################################################################
# Validate Build Variables from Environment
################################################################################
[ ! -f "${GL_MP_SCHEMA_TEMPLATE}" ] && display_failure "${GL_MP_SCHEMA_TEMPLATE} is missing"
[ ! -d "${GL_MP_CONTAINER_CONTEXT}" ] && display_failure "${GL_MP_CHART} does not exist"
[ ! -d "${GL_MP_CHART}" ] && display_failure "${GL_MP_CHART} does not exist"

################################################################################
# Add tarballs with charts to gitlab-mp
################################################################################
display_task "Generating GitLab Marketplace Chart Dependency Tarballs"
if "${GL_MP_BUILD_TARBALLS}"; then
    display_success "complete"
else
    display_failure "failed"
fi

################################################################################
# Update mapping.yaml
################################################################################
display_task "Copying template to operational mapping file"
if cp -a "${GL_MP_SA_MAPPING_YAML_TEMPLATE}" "${GL_MP_SA_MAPPING_YAML}"; then
    display_success "completed"
else
    display_failure "failed"
fi

display_task "Updating Service Account data in mapping.yaml"
if "${GL_MP_UPDATE_SA_MAPPING}"; then
    display_success "mapping.yaml updated"
else
    display_failure "mapping.yaml update failed"
fi

################################################################################
# Update schema.yaml
################################################################################
display_task "Copying template to operational schema file"
if cp -a "${GL_MP_SCHEMA_TEMPLATE}" "${GL_MP_SCHEMA_FILE}"; then
    display_success "completed"
else
    display_failure "failed"
fi

display_task "Updating image tags and RBAC configurations in schema.yaml"
if "${GL_MP_UPDATE_SCHEMA}"; then
    display_success "schema.yaml update complete"
else
    display_failure "schema.yaml updating failed"
fi

################################################################################
# Remove temporary build files
################################################################################
display_task "Remove temporary build files"
post_cleanup

################################################################################
# Build the Deployer Container
################################################################################
# We are not using $TAG and $REGISTRY in the same way as defined in the
# Google GKE Marketplace documentation so we don't pass it here. If we
# were to pass it, it would also have to be in the schema.yaml

docker_build=("docker" "build")
for tag in $(get_container_tags); do
    display_success "build will generate container ${tag}"
    docker_build+=("-t" "${tag}")
done
docker_build+=("${GL_MP_CONTAINER_CONTEXT}")

display_task "Attempting to build GitLab Marketplace deployer container"
if "${docker_build[@]}" > /dev/null 2>&1; then
    display_success "deployer container build complete"
else
    display_failure "deployer container build failed"
fi

display_task "Build Complete!"
