#!/usr/bin/env bash

# Determines the GL_RELEASE_VERSION
# If no tag is found in the push, then revert to using the latest version of
# the Cloud Native GitLab (CNG) Chart

UPSTREAM_CHART_REPO="https://charts.gitlab.io"

bail() {
    echo "$1"
    exit 1
}

set_release() {
    GL_RELEASE_VERSION=$1
    echo "Setting Release Version to ${GL_RELEASE_VERSION}"
    export GL_RELEASE_VERSION=$1
}

# Catch the case that we set this in a pipeline somehow manually
if [ -n "${GL_RELEASE_VERSION}" ]; then
    set_release "${GL_RELEASE_VERSION}"
elif [ -n "${CI_COMMIT_TAG}" ]; then
    set_release "${CI_COMMIT_TAG}"
else
    ! command -v helm > /dev/null 2>&1 && bail "Unable to set GL_RELEASE_VERSION"

    helm init --client-only > /dev/null 2>&1 || bail "Failed to initialize helm"

    helm repo add gitlab "${UPSTREAM_CHART_REPO}"  > /dev/null 2>&1 || bail "Failed to add ${UPSTREAM_CHART_REPO}"

    helm repo update > /dev/null 2>&1 || bail "Failed to update the repository"

    found_version=$(helm search -r "gitlab/gitlab[^-]" --col-width 5|grep -vi "name"|cut -f 2| tr '[:upper:]' '[:lower:]')

    [ -z "${found_version}" ] && bail "GL_RELEASE_VERSION not discovered"

    [ "${found_version}" = "no results found" ] && bail "Failed to find a gitlab chart"

    set_release "${found_version}"
fi
