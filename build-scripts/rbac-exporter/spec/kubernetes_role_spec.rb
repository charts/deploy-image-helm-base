require 'spec_helper'

describe "Kubernetes Role" do
  before do |needs_sample_data|
    @roles = []
    @roles.push(YAML.load_file(File.expand_path("spec/fixtures/role_a.yaml")))
    @roles.push(YAML.load_file(File.expand_path("spec/fixtures/role_b.yaml")))

    @role_bindings = []
    @role_bindings.push(YAML.load_file(File.expand_path("spec/fixtures/bind_role_a_to_account_a.yaml")))
    @role_bindings.push(YAML.load_file(File.expand_path("spec/fixtures/bind_role_b_to_account_b.yaml")))
  end

  context "when initialized with no parameter" do
    it "should raise an error" do
      expect { KubernetesRole.new }.to raise_error(ArgumentError)
    end
  end

  context "when initialized with a role binding hash" do
    it "should create an object" do
      expect(KubernetesRole.new(@role_bindings[0])).to be_an_instance_of(KubernetesRole)
    end

    it "two objects from the same data should be equal" do
      a = KubernetesRole.new @role_bindings[0]
      b = KubernetesRole.new @role_bindings[0]
      expect(a).to eq(b)
    end

    it "two objects from different data should be unequal" do
      a = KubernetesRole.new @role_bindings[0]
      b = KubernetesRole.new @role_bindings[1]
      expect(a).not_to eq(b)
    end
  end

  context "when initialized with a role hash" do
    it "should create an object" do
      expect(KubernetesRole.new(@roles[0])).to be_an_instance_of(KubernetesRole)
    end

    it "two objects from the same data should be equal" do
      a = KubernetesRole.new @roles[0]
      b = KubernetesRole.new @roles[0]
      expect(a).to eq(b)
    end

    it "two objects from different data should be unequal" do
      a = KubernetesRole.new @roles[0]
      b = KubernetesRole.new @roles[1]
      expect(a).not_to eq(b)
    end
  end

  context "when assigned custom rules" do
    it "should match the assigned rule set" do
      a = KubernetesRole.new @roles[0]
      a.define_rules @roles[0]
      expect(a.rules).to eq(@roles[0]["rules"])
    end

    it "should not output unsupported rule types" do
      a = KubernetesRole.new @roles[0]
      a.define_rules @roles[0]
      forbidden = a.rules.map { |r| r.select { |k, _| ["resourceNames"].include? k } }.reject(&:empty?)
      clean = a.config["rules"].map { |r| r.select { |k, _| ["resourceNames"].include? k } }.reject(&:empty?)
      # two expects to ensure both that we have a forbidden type AND that it doesn't exist in config
      # otherwise if someone changes the fixture and removes that type it won't be obvious that the test is broken
      expect(forbidden.length).to be > 0
      expect(clean.length).to be 0
    end
  end
end
