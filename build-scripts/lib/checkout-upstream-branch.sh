#!/usr/bin/env bash
# Script to check out the latest GitLab Cloud Native upstream branch
# If the repository is already cloned, ensure it is the latest master.

set -eo pipefail

GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

[ ! -d "${GL_MP_SCRATCH}" ] && mkdir -p "${GL_MP_SCRATCH}"

needs_cloned="no"

if [ -d "${GL_MP_UPSTREAM_CHART_REPO}" ]; then
    pushd "${GL_MP_UPSTREAM_CHART_REPO}" > /dev/null
    # no need to guard the next statement; if it fails origin_url is going
    # to correct assume that the directory should be removed/replaced with
    # an actual git repository
    origin_url=$(git config --get remote.origin.url)
    popd > /dev/null

    if [ "${origin_url}" != "${GL_MP_UPSTREAM_CHART_URL}" ]; then
        display_task "Attempting to remove ${GL_MP_UPSTREAM_CHART_REPO} [incorrect origin]"

        # safe because we already know this directory exists
        if rm -Rf "${GL_MP_UPSTREAM_CHART_REPO}"; then
            display_success "removal succeeded"
        else
            display_failure "removal failed"
        fi

        needs_cloned="yes"
    else
        pushd "${GL_MP_UPSTREAM_CHART_REPO}" > /dev/null

        display_task "Checking local GitLab Chart repository for updates"
        if git fetch origin > /dev/null 2>&1; then
            display_success "local repository updated successfully"
        else
            display_failure "failed to retrieve updates from ${GL_MP_UPSTREAM_CHART_URL}"
        fi

        display_task "Verifying current branch is ${GITLAB_BRANCH_NAME}"
        if git checkout "${GITLAB_BRANCH_NAME}" > /dev/null 2>&1; then
            display_success "${GITLAB_BRANCH_NAME} is the current branch"
        else
            display_failure "failed to get to the ${GITLAB_BRANCH_NAME} branch"
        fi

        display_task "Updating local branch ${GITLAB_BRANCH_NAME}"
        if git pull origin "${GITLAB_BRANCH_NAME}" > /dev/null 2>&1; then
            display_success "${GITLAB_BRANCH_NAME} branch updated successfully"
        else
            display_failure "${GITLAB_BRANCH_NAME} branch update failed"
        fi

        popd > /dev/null
    fi
else
    needs_cloned="yes"
fi

if [ "${needs_cloned}" = "yes" ]; then
    pushd "${GL_MP_SCRATCH}" > /dev/null

    display_task "Attempting to clone from ${GL_MP_UPSTREAM_CHART_URL}"
    if git clone "${GL_MP_UPSTREAM_CHART_URL}"; then
        display_success "cloning suceeded."
        pushd "${GL_MP_UPSTREAM_CHART_REPO}" > /dev/null

        display_task "Attempting to checkout ${GITLAB_BRANCH_NAME}"
        if git checkout "${GITLAB_BRANCH_NAME}"; then
            display_success "checkout of ${GITLAB_BRANCH_NAME} succeeded"
        else
            display_failure "checkout of ${GITLAB_BRANCH_NAME} failed"
        fi

        popd > /dev/null
    else
        display_failure "cloning failed."
    fi

    popd > /dev/null

fi
